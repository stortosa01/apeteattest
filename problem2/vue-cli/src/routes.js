import Entrantes from './menus/Entrantes.vue';
import Cuchara from './menus/Cuchara.vue';
import Pastas from './menus/Pastas.vue';
import Arroces from './menus/Arroces.vue';
import Ensaladas from './menus/Ensaladas.vue';

export const routes = [
  { path: '/', component: Entrantes },
  { path: '/cuchara', component: Cuchara },
  { path: '/pasta', component: Pastas },
  { path: '/arroces', component: Arroces },
  { path: '/ensaladas', component: Ensaladas }

]